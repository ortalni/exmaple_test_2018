<?php
require "bootstrap.php";
use Chatter\Models\User;

$app = new \Slim\App();
$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});
$app->get('/customers/{number}', function($request, $response,$args){
   return $response->write('Your customer number is '.$args['number']);
});
$app->get('/customers/{cnumber}/products/{pnumber}', function($request, $response,$args){
   return $response->write('Your customer number is '.$args['cnumber'].', and your product is '.$args['pnumber']);
});

$app->get('/users', function($request, $response,$args){
    $_user = new User();
    $users = $_user->all();

    $payload = [];
    foreach($users as $u){
        $payload[$u->id] = [
            "name" => $u->name,
            "phonenumber" => $u->phonenumber,

        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

$app->post('/users', function($request, $response,$args){
    $name = $request->getParsedBodyParam('name','');
    $phonenumber = $request->getParsedBodyParam('phonenumber', '');
    $_user = new User();
    $_user->name = $name;
    $_user->phonenumber = $phonenumber;
    try {
        $_user->save();
    } catch (Illuminate\Database\QueryException $e) {
        var_dump($e->errorInfo);
    }
       
    if($_user->id){
        $payload = ['user_id' => $_user->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});
$app->delete('/users/{user_id}', function($request, $response,$args){
    $user = User::find($args['user_id']);
    $user->delete();
    
    if($user->exists){
        return $response->withStatus(400);
    }
    else{
       return $response->withStatus(200);
   }
});

$app->put('/users/{user_id}', function($request, $response, $args){
    $name = $request->getParsedBodyParam('name','');
    $phonenumber = $request->getParsedBodyParam('phonenumber','');
    $_user = User::find($args['user_id']);
    $_user->name = $name; 
    $_user->phonenumber = $phonenumber;

   
    if($_user->save()){
        $payload = ['user_id' => $_user->id,"result" => "The user has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->post('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();

    User::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});


$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();